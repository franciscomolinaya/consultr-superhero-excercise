import { getHeroes } from "../../service";
import { FETCH_HEROES } from "../types";

export const getHeroesList = () => {
    return {
        type: FETCH_HEROES,
        payload: getHeroes()
    }
}