import { FETCH_HEROES_FULFILLED, FETCH_HEROES_PENDING, FETCH_HEROES_REJECTED } from "../types";

const initialState = {
    heroes_data: [],
    fetching: false,
    error: undefined

}
const MainReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_HEROES_FULFILLED:
            return {
                heroes_data: action.payload,
                fetching: false,
                error: undefined,
            }
        case FETCH_HEROES_PENDING:
            return {
                ...state,
                fetching: true,
                error: undefined
            }
        case FETCH_HEROES_REJECTED:
            return {
                heroes_data: [],
                fetching: false,
                error: action.payload
            }
        default:
            return state;
    }
}

export default MainReducer;