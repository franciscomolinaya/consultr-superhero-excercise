const base_url = 'https://akabab.github.io/superhero-api/api/';

export const getHeroes = async () => {
    const heroes_list = await fetch(`${base_url}all.json`)
    .then(response => response.json());
    return heroes_list;
}