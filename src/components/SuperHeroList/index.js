import React, { useEffect } from 'react';
import { View, Text, FlatList, RefreshControl } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { getHeroesList } from '../../redux/actions';
import HeroCell from '../HeroCell';
import Styles from './styles';

function SuperHeroList() {
    const { heroes_data, fetching, error } = useSelector((state) => state);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getHeroesList());
    }, []);

    const renderList = heroes_data.length > 0 && !fetching && !error;

    const refreshControl = () => {
        const fetchHeroes = () => dispatch(getHeroesList());
        return (
<RefreshControl
                        colors={["black", "black"]}
                        refreshing={fetching}
                        onRefresh={fetchHeroes} />
            
        )
    }
    

    return (
        <View style={Styles.wrapper}>

            {
                renderList &&
                <FlatList
                    data={heroes_data}
                    style={Styles.flatList}
                    numColumns={2}
                    keyExtractor={({ title }, index) => `${title}-${index}`}
                    renderItem={HeroCell}
                    refreshControl={refreshControl()}
                    />    
            }

            {
                fetching &&
                <Text style={Styles.heading}>Loading</Text>
            }
            {
                error &&
                <Text style={Styles.heading}>Error</Text>
            }
        </View>
    )
}

export default SuperHeroList;