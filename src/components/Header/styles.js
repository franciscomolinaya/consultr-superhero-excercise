import { StyleSheet } from "react-native";

export default Styles = StyleSheet.create({
    wrapper: {
        display: 'flex',
        alignItems: 'center',
    },
    title: {
        fontSize: 48,
        color: '#ccc'
    }
});