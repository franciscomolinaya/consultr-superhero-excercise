import React from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import Styles from './styles';

function HeroCell({ item }) {
    const { name, appearance, images } = item;
    const { weight, height } = appearance;
    const { md } = images;

    return (
        <TouchableOpacity style={Styles.wrapper}>
            <Image source={{ uri: md }} style={Styles.image} />
            <View style={Styles.contentWrapper}>
                <Text style={Styles.title}>{`${name}`}</Text>
                <Text style={Styles.subTitle}>{`Height: ${height[1]}`}</Text>
                <Text style={Styles.subTitle}>{`Weight: ${weight[1]}`}</Text>
            </View>
        </TouchableOpacity>
    )
}

export default HeroCell;
