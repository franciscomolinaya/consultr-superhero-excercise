import React from 'react'
import { View, Text } from 'react-native';
import Styles from './styles';

function Header() {

    return (
        <View style={Styles.wrapper}>
            <Text style={Styles.title}>Superhero App</Text>
        </View>
    )
}

export default Header;
