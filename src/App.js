import React from 'react';
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  useColorScheme,
} from 'react-native';
import { Provider } from 'react-redux';
import Header from './components/Header';
import SuperHeroList from './components/SuperHeroList';
import ContextProvider from './context/context';
import store from './redux';

const App = () => {
  
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    display: 'flex',
    flex: 1,
    backgroundColor: isDarkMode ? '#000' : '#fff',
  };
  const statusBarStyle = isDarkMode ? 'light-content' : 'dark-content';


  return (
    <Provider store={store}>
    <ContextProvider>
    <SafeAreaView style={backgroundStyle}>
      <StatusBar barStyle={statusBarStyle} />
        <Header />
        <SuperHeroList/>
    </SafeAreaView>
    </ContextProvider>
    </Provider>
  );
};

export default App;
