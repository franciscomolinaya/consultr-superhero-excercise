import React, { createContext, useState } from 'react';
import { useColorScheme } from 'react-native';


export const Context = createContext();


function ContextProvider({ children }) {
    const [value, setvalue] = useState({ theme: useColorScheme() });
    return <Context.Provider value={value}>
        {children}
    </Context.Provider>
}

export default ContextProvider;
