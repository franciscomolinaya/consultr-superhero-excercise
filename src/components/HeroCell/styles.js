import { StyleSheet } from "react-native";

export default Styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        aspectRatio: 1,
        margin: 4,
        minHeight: 300,        
    },
    contentWrapper: {
        position: 'absolute',
        bottom: 12,
        left: 12
    },
    title: {
        fontSize: 32,
        fontWeight: 'bold',
        color: 'white',
    },
    subTitle: {
        fontSize: 18,
        color: 'white',
    },
    image: {
        flex: 1,
        overflow: 'hidden',
        borderRadius: 12,
        resizeMode: 'cover',
    }
})