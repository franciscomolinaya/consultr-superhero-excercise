import { StyleSheet } from "react-native"

export default Styles = StyleSheet.create({
    wrapper: {
        flex: 1
    },
    flatList: {
        flex: 1,
        marginTop: 12,
        borderTopWidth: 1,
        paddingTop: 12,
        borderTopColor: '#ccc'
    },
    heading: {
        fontSize: 48,
        color: '#ccc'
    }
})